<?php 
require_once('dbconfig.php');
error_reporting(0);
	$sqlTahunIni = "select id from periode where periode='".date('Y')."'";
    $queryTahunIni = $pdo->query($sqlTahunIni);
    $viewTahunIni = $queryTahunIni->fetch(PDO::FETCH_ASSOC);
    $tahun = (isset($_POST['buttonLihat'])?$_POST['tahun']:$viewTahunIni['id']);

    if(isset($_POST['buttonLihat'])){
        $sqlPeriode = "select periode from periode where id='".$_POST['tahun']."'";
        $queryPeriode = $pdo->query($sqlPeriode);
        $viewPeriode = $queryPeriode->fetch(PDO::FETCH_ASSOC); 

        $periodeTahun = $viewPeriode['periode'];
    }else{
        $periodeTahun = date('Y');
    }

	$sqlDivisi = "select id_divisi,divisi from divisi order by id asc";
    $queryDivisi = $pdo->query($sqlDivisi);
    while($viewDivisi = $queryDivisi->fetch(PDO::FETCH_ASSOC)) {
        $arrDivisi[] = $viewDivisi['divisi'];

        $sqlJum = "select sum(Available)as jumlah_available, sum(Assigned)as jumlah_assigned
                from datas where periode_id='".$tahun."' and divisi_id='".$viewDivisi['id_divisi']."'";
        $queryJum = $pdo->query($sqlJum);
        $viewJum = $queryJum->fetch(PDO::FETCH_ASSOC);

        $jumAvailable = (empty($viewJum['jumlah_available'])?0:$viewJum['jumlah_available']);
        $jumAssigned = (empty($viewJum['jumlah_assigned'])?0:$viewJum['jumlah_assigned']);

        $arrJumAvailable[] = $jumAvailable;
        $arrJumAssigned[] = $jumAssigned; 

        $dataTabeel[] = [
            'id' => $viewDivisi['id_divisi'],
            'periode' => $periodeTahun,
            'divisi' => $viewDivisi['divisi'],
            'available' => $jumAvailable,
            'assgined' => $jumAssigned
        ];       
    }
	$arrJumlah[] = ['name' => 'Available', 'data' => $arrJumAvailable];
    $arrJumlah[] = ['name' => 'Assigned', 'data' => $arrJumAssigned];

    $jsonDivisi = json_encode($arrDivisi);
    $jsonData = json_encode($arrJumlah, JSON_NUMERIC_CHECK);	
?>
<?php include_once 'header.php'; ?>
<div class="container">		
	<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form method="post" action="tabel.php">
                <div class="input-group">
                    <select class="form-control" name="tahun">
					<option>Pilih Tahun</option>
					<?php
                            $sqlPeriode = "select * from periode order by id asc";
                            $queryPeriode = $pdo->query($sqlPeriode);
                            while($viewPeriode = $queryPeriode->fetch(PDO::FETCH_ASSOC)){
                                echo "<option value='".$viewPeriode['id']."'>".$viewPeriode['periode']."</option>";
                            }
                        ?>
				</select>
                    <span class="input-group-btn">                        
						<button type="submit" name="buttonLihat" class="btn btn-primary"><class="btn btn-large btn-warning"><i class="glyphicon glyphicon-backward"></i> Lihat</button>
                    </span>						
				</div>
        </div>
            </form>
    </div>
 </div>    
	<br/>	
	<table class="table table-hover">
		<tr>
			<th>No</th>
			<th>Tahun</th>
			<th>Divisi</th>
			<th>Available</th>
			<th>Assigned</th>			
		</tr>
			
	</tr>
	<tbody>
<?php
$no=1;
foreach ($dataTabeel as $key) {
?>
            <tr data-toggle="collapse" data-target="#table<?= $no ?>" class="accordion-toggle">
                <td><?php echo $no ?></td>
				<td><?php echo $key['periode']; ?></td>				
				<td><?php echo $key['divisi']; ?>;</td>
                <td><?php echo $key['available']; ?></td>
                <td><?php echo $key['assgined']; ?></td>                
            </tr>
            <tr id="table<?= $no ?>" class="collapse">
                <td colspan="5">
                    <table class="table" style="background-color: #d7d7d7;">
                        <?php
                            $sql = "select * from datas where periode_id='".$tahun."' and divisi_id='".$key['id']."'
                                     order by id asc";
                            $query = $pdo->query($sql);
                            while($view = $query->fetch(PDO::FETCH_ASSOC)){
                        ?>
                        <tr>
                            <td colspan="3" width="72%">&nbsp;</td>
                            <td width="14%"><?php echo $view['Available']; ?></td>
                            <td width="14%"><?php echo $view['Assigned']; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
<?php
    $no++;
}
?>
        </tbody>
</table>
	</table>	
</div>

<div id="container" style="min-width: 310px; height: 1000px; margin: 0 auto"></div>

<script type="text/javascript">
        var dataLabelX = <?php echo $jsonDivisi; ?>;
        var dataGrafik = <?php echo $jsonData ; ?>;

        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Stacked bar chart'
            },
            xAxis: {
                categories: dataLabelX
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: dataGrafik
        });
    </script>
